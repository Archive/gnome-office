/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnomeoffice/go-plugin.h>

#define PARENT_TYPE G_TYPE_OBJECT

struct _GOfficePluginPrivate {
	gchar *id_str;
	GList *services;
	GOfficePluginCleanupFunc cleanup_func;
	gboolean cleanup_done;
};

static void gop_class_init (GOfficePluginClass *klass);
static void gop_init (GOfficePlugin *plugin, GOfficePluginClass *klass);
static void gop_finalize (GObject *object);

static GObjectClass *parent_class = NULL;

static void
gop_class_init (GOfficePluginClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = gop_finalize;
}

static void
gop_init (GOfficePlugin *plugin, GOfficePluginClass *klass)
{
	plugin->priv = g_new0 (GOfficePluginPrivate, 1);
}

static void
gop_finalize (GObject *object)
{
	GOfficePlugin *plugin = (GOfficePlugin *) object;

	/* do cleanup if it hasn't been done */
	if (!plugin->priv->cleanup_done)
		goffice_plugin_cleanup (plugin, NULL);

	/* free memory */
	if (plugin->priv->id_str) {
		g_free (plugin->priv->id_str);
		plugin->priv->id_str = NULL;
	}

	g_list_foreach (plugin->priv->services, (GFunc) g_object_unref, NULL);
	g_list_free (plugin->priv->services);
	plugin->priv->services = NULL;

	g_free (plugin->priv);
	plugin->priv = NULL;

	/* call parent class */
	parent_class->finalize (object);
}

GType
goffice_plugin_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GOfficePluginClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gop_class_init,
                        NULL,
                        NULL,
                        sizeof (GOfficePlugin),
                        0,
                        (GInstanceInitFunc) gop_init
                };
                type = g_type_register_static (G_TYPE_OBJECT, "GOfficePlugin", &info, 0);
        }
        return type;
}

/**
 * goffice_plugin_new
 * @id_str: ID for the new plugin.
 *
 * Create a new #GOfficePlugin object, which allows you to write GNOME
 * Office plugins, which will be automatically understood by all GNOME
 * Office applications.
 *
 * Returns: a pointer to the newly-created object.
 */
GOfficePlugin *
goffice_plugin_new (const gchar *id_str, GOfficePluginCleanupFunc cleanup_func)
{
	GOfficePlugin *plugin;

	plugin = g_object_new (GOFFICE_TYPE_PLUGIN, 0);
	plugin->priv->id_str = g_strdup (id_str);
	plugin->priv->cleanup_func = cleanup_func;

	return plugin;
}

/**
 * goffice_plugin_get_id
 * @plugin: A #GOfficePlugin object.
 *
 * Get the ID of the given plugin. This ID is used to uniquely identify
 * the plugin in the system.
 *
 * Returns: the unique ID for this plugin.
 */
const gchar *
goffice_plugin_get_id (GOfficePlugin *plugin)
{
	g_return_val_if_fail (GOFFICE_IS_PLUGIN (plugin), NULL);
	return (const gchar *) plugin->priv->id_str;
}

/**
 * goffice_plugin_cleanup
 * @plugin: A #GOfficePlugin object.
 * @ret_error: Error returned on failure.
 *
 * Clean up (unloads) the given plugin.
 */
gboolean
goffice_plugin_cleanup (GOfficePlugin *plugin, GError **ret_error)
{
	g_return_val_if_fail (GOFFICE_IS_PLUGIN (plugin), FALSE);

	if (plugin->priv->cleanup_done) {
		if (ret_error) {
			*ret_error = g_error_new_literal (
				g_quark_from_string (LIBGNOMEOFFICE_ERROR_DOMAIN),
				-1, "Cleanup already done on this plugin");
		}

		return FALSE;
	}

	if (plugin->priv->cleanup_func)
		return plugin->priv->cleanup_func (plugin, ret_error);
	else {
		if (ret_error) {
			*ret_error = g_error_new_literal (
				g_quark_from_string (LIBGNOMEOFFICE_ERROR_DOMAIN),
				-1, "Method not implemented");
		}
	}

	return FALSE;
}

/**
 * goffice_plugin_add_service
 * @plugin: A #GOfficePlugin object.
 * @service: The service to be added to the given plugin.
 *
 * Add a service to the given plugin.
 */
void
goffice_plugin_add_service (GOfficePlugin *plugin, GOfficeService *service)
{
	g_return_if_fail (GOFFICE_IS_PLUGIN (plugin));
	g_return_if_fail (GOFFICE_IS_SERVICE (service));

	g_object_ref (G_OBJECT (service));
	plugin->priv->services = g_list_append (plugin->priv->services, service);
}

/**
 * goffice_plugin_remove_service
 * @plugin: A #GOfficePlugin object.
 * @service: The service to be removed.
 *
 * Remove the given service from the plugin.
 */
void
goffice_plugin_remove_service (GOfficePlugin *plugin, GOfficeService *service)
{
	GList *l;

	g_return_if_fail (GOFFICE_IS_PLUGIN (plugin));
	g_return_if_fail (GOFFICE_IS_SERVICE (service));

	for (l = plugin->priv->services; l; l = l->next) {
		GOfficeService *tmp = GOFFICE_SERVICE (l->data);

		if (tmp == service) {
			plugin->priv->services = g_list_remove (plugin->priv->services, tmp);
			g_object_unref (G_OBJECT (tmp));

			break;
		}
	}
}

/**
 * goffice_plugin_get_services
 * @plugin: A #GOfficePlugin object.
 *
 * Get the list of services implemented by the given plugin. The
 * returned value points to static data, so YOU SHOULD NOT FREE IT.
 *
 * Returns: a list of #GOfficeService.
 */
const GList *
goffice_plugin_get_services (GOfficePlugin *plugin)
{
	g_return_val_if_fail (GOFFICE_IS_PLUGIN (plugin), NULL);
	return (const GList *) plugin->priv->services;
}

/**
 * goffice_plugin_get_services_of_type
 * @plugin: A #GOfficePlugin object.
 * @GType: The type to query for.
 *
 * Get the list of services of the given type implemented by the
 * given plugin. Opposite to @goffice_plugin_get_services, the
 * returned GList should be freed. But, take attention to only free
 * the list, not the nodes' contents, which are pointers to static
 * data.
 *
 * Returns: a list of #GOfficeService.
 */
GList *
goffice_plugin_get_services_of_type (GOfficePlugin *plugin, GType type)
{
	GList *list = NULL;
	GList *l;

	g_return_val_if_fail (GOFFICE_IS_PLUGIN (plugin), NULL);

	for (l = plugin->priv->services; l != NULL; l = l->next) {
		GOfficeService *service = GOFFICE_SERVICE (l->data);
		if (G_TYPE_CHECK_INSTANCE_TYPE (service, type))
			list = g_list_append (list, service);
	}

	return list;
}
