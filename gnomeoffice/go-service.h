/* GNOME Office library
 * Copyright (C) 2002-2003 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_OFFICE_SERVICE_H__
#define __GNOME_OFFICE_SERVICE_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define GOFFICE_TYPE_SERVICE        (goffice_service_get_type ())
#define GOFFICE_SERVICE(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GOFFICE_TYPE_SERVICE, GOfficeService))
#define GOFFICE_SERVICE_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_SERVICE, GOfficeServiceClass))
#define GOFFICE_IS_SERVICE(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GOFFICE_TYPE_SERVICE))
#define GOFFICE_IS_SERVICE_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_SERVICE))

typedef struct _GOfficeService      GOfficeService;
typedef struct _GOfficeServiceClass GOfficeServiceClass;

struct  _GOfficeService {
	GObject object;
};

struct _GOfficeServiceClass {
	GObjectClass parent_class;

	/* virtual methods */
	const gchar * (* service_get_description) (GOfficeService *service);
	const gchar * (* service_get_help) (GOfficeService *service);
};

GType        goffice_service_get_type (void);
const gchar *goffice_service_get_description (GOfficeService *service);
const gchar *goffice_service_get_help (GOfficeService *service);

G_END_DECLS

#endif
