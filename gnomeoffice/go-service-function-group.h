/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_OFFICE_SERVICE_FUNCTION_GROUP_H__
#define __GNOME_OFFICE_SERVICE_FUNCTION_GROUP_H__

#include <gnomeoffice/go-service.h>

G_BEGIN_DECLS

#define GOFFICE_TYPE_SERVICE_FUNCTION_GROUP        (goffice_service_function_group_get_type ())
#define GOFFICE_SERVICE_FUNCTION_GROUP(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GOFFICE_TYPE_SERVICE_FUNCTION_GROUP, GOfficeServiceFunctionGroup))
#define GOFFICE_SERVICE_FUNCTION_GROUP_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_SERVICE_FUNCTION_GROUP, GOfficeServiceFunctionGroupClass))
#define GOFFICE_IS_SERVICE_FUNCTION_GROUP(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GOFFICE_TYPE_SERVICE_FUNCTION_GROUP))
#define GOFFICE_IS_SERVICE_FUNCTION_GROUP_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_SERVICE_FUNCTION_GROUP))

typedef struct _GOfficeServiceFunctionGroup        GOfficeServiceFunctionGroup;
typedef struct _GOfficeServiceFunctionGroupClass   GOfficeServiceFunctionGroupClass;
typedef struct _GOfficeServiceFunctionGroupPrivate GOfficeServiceFunctionGroupPrivate;

typedef struct {
	gchar *function_name;
	GType return_value;
	GArray *argument_types;
	GPtrArray *argument_names;
	gchar *help;
} GOfficeServiceFunctionInfo;

struct _GOfficeServiceFunctionGroup {
	GOfficeService service;
	GOfficeServiceFunctionGroupPrivate *priv;
};

struct _GOfficeServiceFunctionGroupClass {
	GOfficeServiceClass parent_class;

	/* virtual methods */
	const gchar * (* service_get_category) (GOfficeServiceFunctionGroup *service);
	GValue *      (* service_eval) (GOfficeServiceFunctionGroup *service,
					const gchar *fn_name, const GValueArray *values);
};

GType        goffice_service_function_group_get_type (void);
const gchar *goffice_service_function_group_get_category (GOfficeServiceFunctionGroup *service);
GList       *goffice_service_function_group_get_names (GOfficeServiceFunctionGroup *service);
void         goffice_service_function_group_register_function (GOfficeServiceFunctionGroup *service,
							       const gchar *fn_name,
							       GType ret_value,
							       const GType *arg_types,
							       const gchar **arg_names,
							       const gchar *help);
const GOfficeServiceFunctionInfo *
goffice_service_function_group_get_function_info (GOfficeServiceFunctionGroup *service,
						  const gchar *fn_name);
GValue      *goffice_service_function_group_eval (GOfficeServiceFunctionGroup *service,
						  const gchar *fn_name,
						  const GValueArray *values);

G_END_DECLS

#endif
