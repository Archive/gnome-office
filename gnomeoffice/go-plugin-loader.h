/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_OFFICE_PLUGIN_LOADER_H__
#define __GNOME_OFFICE_PLUGIN_LOADER_H__

#include <gnomeoffice/go-plugin.h>

G_BEGIN_DECLS

#define GOFFICE_TYPE_PLUGIN_LOADER        (goffice_plugin_loader_get_type ())
#define GOFFICE_PLUGIN_LOADER(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GOFFICE_TYPE_PLUGIN_LOADER, GOfficePluginLoader))
#define GOFFICE_PLUGIN_LOADER_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_PLUGIN_LOADER, GOfficePluginLoaderClass))
#define GOFFICE_IS_PLUGIN_LOADER(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GOFFICE_TYPE_PLUGIN_LOADER))
#define GOFFICE_IS_PLUGIN_LOADER_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_PLUGIN_LOADER))

typedef struct _GOfficePluginLoader        GOfficePluginLoader;
typedef struct _GOfficePluginLoaderClass   GOfficePluginLoaderClass;
typedef struct _GOfficePluginLoaderPrivate GOfficePluginLoaderPrivate;

struct _GOfficePluginLoader {
	GObject object;
	GOfficePluginLoaderPrivate *priv;
};

struct _GOfficePluginLoaderClass {
	GObjectClass parent_class;
};

GType                goffice_plugin_loader_get_type (void);
GOfficePluginLoader *goffice_plugin_loader_new (void);
GList               *goffice_plugin_loader_get_ids (GOfficePluginLoader *loader);
GOfficePlugin       *goffice_plugin_loader_activate (GOfficePluginLoader *loader,
						     const gchar *id);

G_END_DECLS

#endif
