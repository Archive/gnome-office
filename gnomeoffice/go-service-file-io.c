/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnomeoffice/go-service-file-io.h>

#define PARENT_TYPE GOFFICE_TYPE_SERVICE
#define FILE_IO_CLASS(service) GOFFICE_SERVICE_FILE_IO_CLASS (G_OBJECT_GET_CLASS (service))

static void gopsfio_class_init (GOfficeServiceFileIoClass *klass);
static void gopsfio_init (GOfficeServiceFileIo *service,
			  GOfficeServiceFileIoClass *klass);
static void gopsfio_finalize (GObject *object);

static GObjectClass *parent_class = NULL;

static void
gopsfio_class_init (GOfficeServiceFileIoClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gopsfio_finalize;
	klass->plugin_file_open = NULL;
	klass->plugin_file_save = NULL;
}

static void
gopsfio_init (GOfficeServiceFileIo *service,
	      GOfficeServiceFileIoClass *klass)
{
}

static void
gopsfio_finalize (GObject *object)
{
	parent_class->finalize (object);
}

GType
goffice_service_file_io_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GOfficeServiceFileIoClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gopsfio_class_init,
                        NULL,
                        NULL,
                        sizeof (GOfficeServiceFileIo),
                        0,
                        (GInstanceInitFunc) gopsfio_init
                };
                type = g_type_register_static (PARENT_TYPE,
					       "GOfficeServiceFileIo",
					       &info, 0);
        }
        return type;
}

/**
 * goffice_service_file_io_open
 * @service: a #GOfficeServiceFileIO object.
 * @input: the input object.
 * @error:
 *
 * Open a given input object with the given plugin service.
 *
 * Returns: TRUE if successful, FALSE otherwise.
 */
gboolean
goffice_service_file_io_open (GOfficeServiceFileIo *service,
			      GsfInput *input,
			      GError **error)
{
	g_return_val_if_fail (GOFFICE_IS_SERVICE_FILE_IO (service), FALSE);
	g_return_val_if_fail (GSF_IS_INPUT (input), FALSE);
	g_return_val_if_fail (FILE_IO_CLASS (service)->plugin_file_open != NULL, FALSE);

	return FILE_IO_CLASS (service)->plugin_file_open (service, input, error);
}

/**
 * goffice_service_file_io_save
 * @service: a #GOfficeServiceFileIO object.
 * @output: the output object.
 * @error:
 *
 * Save a given output object with the given plugin service.
 *
 * Returns: TRUE if successful, FALSE otherwise.
 */
gboolean
goffice_service_file_io_save (GOfficeServiceFileIo *service,
			      GsfOutput *output,
			      GError **error)
{
	g_return_val_if_fail (GOFFICE_IS_SERVICE_FILE_IO (service), FALSE);
	g_return_val_if_fail (GSF_IS_OUTPUT (output), FALSE);
	g_return_val_if_fail (FILE_IO_CLASS (service)->plugin_file_save != NULL, FALSE);

	return FILE_IO_CLASS (service)->plugin_file_save (service, output, error);
}
