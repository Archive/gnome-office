/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnomeoffice/go-service-function-group.h>
#include <glib/gi18n.h>

#define PARENT_TYPE GOFFICE_TYPE_SERVICE
#define FUNCTION_GROUP_CLASS(service) GOFFICE_SERVICE_FUNCTION_GROUP_CLASS (G_OBJECT_GET_CLASS (service))

struct _GOfficeServiceFunctionGroupPrivate {
	GHashTable *functions;
};

static void gopsfg_class_init (GOfficeServiceFunctionGroupClass *klass);
static void gopsfg_init       (GOfficeServiceFunctionGroup *service,
			       GOfficeServiceFunctionGroupClass *klass);
static void gopsfg_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

static void
gopsfg_class_init (GOfficeServiceFunctionGroupClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = gopsfg_finalize;
	klass->service_get_category = NULL;
	klass->service_eval = NULL;
}

static void
gopsfg_init (GOfficeServiceFunctionGroup *service,
	     GOfficeServiceFunctionGroupClass *klass)
{
	g_return_if_fail (GOFFICE_IS_SERVICE_FUNCTION_GROUP (service));

	service->priv = g_new0 (GOfficeServiceFunctionGroupPrivate, 1);
	service->priv->functions = g_hash_table_new (g_str_hash, g_str_equal);
}

static void
free_function_info (gpointer key, gpointer value, gpointer user_data)
{
	GOfficeServiceFunctionInfo *info = value;

	g_free (info->function_name);
	g_array_free (info->argument_types, TRUE);
	g_ptr_array_free (info->argument_names, TRUE);
	g_free (info->help);

	g_free (info);
}

static void
gopsfg_finalize (GObject *object)
{
	GOfficeServiceFunctionGroup *service = object;

	g_return_if_fail (GOFFICE_IS_SERVICE_FUNCTION_GROUP (service));

	if (service->priv) {
		if (service->priv->functions) {
			g_hash_table_foreach (service->priv->functions, (GHFunc) free_function_info, NULL);
			g_hash_table_destroy (service->priv->functions);
			service->priv->functions = NULL;
		}

		g_free (service->priv);
		service->priv = NULL;
	}

	parent_class->finalize (object);
}

GType
goffice_service_function_group_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GOfficeServiceFunctionGroupClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gopsfg_class_init,
                        NULL,
                        NULL,
                        sizeof (GOfficeServiceFunctionGroup),
                        0,
                        (GInstanceInitFunc) gopsfg_init
                };
                type = g_type_register_static (PARENT_TYPE,
					       "GOfficeServiceFunctionGroup",
					       &info, 0);
        }
        return type;
}

/**
 * goffice_service_function_group_get_category
 * @service: A #GOfficeServiceFunctionGroup object.
 *
 * Get the category of this function group. The category is just
 * a string which can be used to group different function groups
 * by category.
 *
 * Returns: the category for this function group.
 */
const gchar *
goffice_service_function_group_get_category (GOfficeServiceFunctionGroup *service)
{
	g_return_val_if_fail (GOFFICE_IS_SERVICE_FUNCTION_GROUP (service), NULL);

	if (FUNCTION_GROUP_CLASS (service)->service_get_category)
		return FUNCTION_GROUP_CLASS (service)->service_get_category (service);

	return NULL;
}

static void
add_function_name (gpointer key, gpointer value, gpointer user_data)
{
	GList **names = (GList **) user_data;

	*names = g_list_append (*names, g_strdup (key));
}

/**
 * goffice_service_function_group_get_names
 * @service: A #GOfficeServiceFunctionGroup object.
 *
 * Get the names of the functions the given plugin provides. Then
 * with each name returned, you can call
 * #goffice_service_function_group_get_function_info to
 * retrieve more information about that given function.
 *
 * Returns: a GList containing the names of all functions. After
 * using it, you should free this list, by calling
 * #goffice_util_free_string_list.
 */
GList *
goffice_service_function_group_get_names (GOfficeServiceFunctionGroup *service)
{
	GList *names = NULL;

	g_return_val_if_fail (GOFFICE_IS_SERVICE_FUNCTION_GROUP (service), NULL);

	g_hash_table_foreach (service->priv->functions, (GHFunc) add_function_name, &names);
	return names;
}

/**
 * goffice_service_function_group_register_function
 * @service: A #GOfficeServiceFunctionGroup object.
 * @fn_name: Name for the function to register.
 * @ret_value: Return value type.
 * @arg_types: Types for arguments.
 * @arg_names: Names for arguments.
 * @help: Help text for the function.
 *
 * Register a new function in the function group service, thus making
 * it available for users of this given service.
 */
void
goffice_service_function_group_register_function (GOfficeServiceFunctionGroup *service,
						  const gchar *fn_name,
						  GType ret_value,
						  const GType *arg_types,
						  const gchar **arg_names,
						  const gchar *help)
{
	GOfficeServiceFunctionInfo *info;
	gint i;

	g_return_if_fail (GOFFICE_IS_SERVICE_FUNCTION_GROUP (service));
	g_return_if_fail (fn_name != NULL);

	/* see if we already have a function with this name */
	if (g_hash_table_lookup (service->priv->functions, fn_name)) {
		g_warning (_("Function %s has already been registered in function group"), fn_name);
		return;
	}

	/* check that all parameters are correct */
	if (arg_types && arg_names) {
		if ((sizeof (arg_types) / sizeof (arg_types[0]))
		    != (sizeof (arg_names) / sizeof (arg_names[0]))) {
			g_warning (_("Unmatched number of argument types/names for function %s"), fn_name);
			return;
		}
	} else if (!(!arg_types && !arg_names)) {
		g_warning (_("Unmatched number of argument types/names for function %s"), fn_name);
		return;
	}

	/* fill in a new function info structure */
	info = g_new0 (GOfficeServiceFunctionInfo, 1);
	info->function_name = g_strdup (fn_name);
	info->return_value = ret_value;
	info->help = g_strdup (help);

	info->argument_types = g_array_new (FALSE, FALSE, sizeof (GType));
	info->argument_names = g_ptr_array_new ();
	if (arg_types) {
		for (i = 0; i < (sizeof (arg_types) / sizeof (arg_types[0])); i++) {
			g_array_append_val (info->argument_types, arg_types[i]);
			g_ptr_array_add (info->argument_names, g_strdup (arg_names[i]));
		}
	}

	g_hash_table_insert (service->priv->functions, info->function_name, info);
}

/**
 * goffice_service_function_group_get_function_info
 * @service: A #GOfficeServiceFunctionGroup object.
 * @fn_name: Name of the function.
 *
 * Retrieve information about a given function. The returned value
 * (of type GOfficeServiceFunctionInfo) contais all needed information
 * about the function specified in the @fn_name parameter. The returned
 * value is a pointer to a private structure, so you shouldn't free it.
 *
 * Returns: a #GOfficeServiceFunctionInfo structure on success, NULL
 * if there was any error (function not found, method not implemented
 * on the underlying plugin, etc).
 */
const GOfficeServiceFunctionInfo *
goffice_service_function_group_get_function_info (GOfficeServiceFunctionGroup *service,
						  const gchar *fn_name)
{
	g_return_val_if_fail (GOFFICE_IS_SERVICE_FUNCTION_GROUP (service), NULL);
	g_return_val_if_fail (fn_name != NULL, NULL);

	return (const GOfficeServiceFunctionInfo *) g_hash_table_lookup (service->priv->functions, fn_name);
}

/**
 * goffice_service_function_group_eval
 * @service: A #GOfficeServiceFunctionGroup object.
 * @fn_name: Name of the function to invoke.
 * @values: Values for function arguments.
 *
 * Call a function on the function group service.
 *
 * Returns: a GValue containing the return code from the function,
 * or NULL on error.
 */
GValue *
goffice_service_function_group_eval (GOfficeServiceFunctionGroup *service,
				     const gchar *fn_name,
				     const GValueArray *values)
{
	g_return_val_if_fail (GOFFICE_IS_SERVICE_FUNCTION_GROUP (service), NULL);
	g_return_val_if_fail (fn_name != NULL, NULL);

	if (FUNCTION_GROUP_CLASS (service)->service_eval)
		return FUNCTION_GROUP_CLASS (service)->service_eval (service, fn_name, values);

	return NULL;
}
