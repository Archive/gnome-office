/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnomeoffice/go-service.h>

#define PARENT_TYPE G_TYPE_OBJECT
#define SERVICE_CLASS(service) GOFFICE_SERVICE_CLASS (G_OBJECT_GET_CLASS (service))

static void gops_class_init (GOfficeServiceClass *klass);
static void gops_init (GOfficeService *service, GOfficeServiceClass *klass);
static void gops_finalize (GObject *object);

static GObjectClass *parent_class = NULL;

static void
gops_class_init (GOfficeServiceClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = gops_finalize;
	klass->service_get_description = NULL;
	klass->service_get_help = NULL;
}

static void
gops_init (GOfficeService *service, GOfficeServiceClass *klass)
{
}

static void
gops_finalize (GObject *object)
{
	parent_class->finalize (object);
}

GType
goffice_service_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GOfficeServiceClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gops_class_init,
                        NULL,
                        NULL,
                        sizeof (GOfficeService),
                        0,
                        (GInstanceInitFunc) gops_init
                };
                type = g_type_register_static (G_TYPE_OBJECT, "GOfficeService", &info, 0);
        }
        return type;
}

/**
 * goffice_service_get_description
 * @service: A #GOfficeService object.
 *
 * Get a short description from the given plugin.
 *
 * Returns: a short description of what the plugin does.
 */
const gchar *
goffice_service_get_description (GOfficeService *service)
{
	g_return_val_if_fail (GOFFICE_IS_SERVICE (service), NULL);

	if (SERVICE_CLASS (service)->service_get_description)
		return SERVICE_CLASS (service)->service_get_description (service);

	return NULL;
}

/**
 * goffice_service_get_help
 * @service: A #GOfficeService object.
 *
 * Get complete description of the given plugin.
 *
 * Returns: a long description of what the plugin does.
 */
const gchar *
goffice_service_get_help (GOfficeService *service)
{
	g_return_val_if_fail (GOFFICE_IS_SERVICE (service), NULL);

	if (SERVICE_CLASS (service)->service_get_help)
		return SERVICE_CLASS (service)->service_get_help (service);

	return NULL;
}
