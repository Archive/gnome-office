/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_OFFICE_PLUGIN_H__
#define __GNOME_OFFICE_PLUGIN_H__

#include <gnomeoffice/go-service.h>

G_BEGIN_DECLS

#define GOFFICE_TYPE_PLUGIN        (goffice_plugin_get_type ())
#define GOFFICE_PLUGIN(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GOFFICE_TYPE_PLUGIN, GOfficePlugin))
#define GOFFICE_PLUGIN_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_PLUGIN, GOfficePluginClass))
#define GOFFICE_IS_PLUGIN(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GOFFICE_TYPE_PLUGIN))
#define GOFFICE_IS_PLUGIN_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_PLUGIN))

typedef struct _GOfficePlugin        GOfficePlugin;
typedef struct _GOfficePluginClass   GOfficePluginClass;
typedef struct _GOfficePluginPrivate GOfficePluginPrivate;

struct _GOfficePlugin {
	GObject object;
	GOfficePluginPrivate *priv;
};

struct _GOfficePluginClass {
	GObjectClass parent_class;
};

typedef gboolean (* GOfficePluginCleanupFunc) (GOfficePlugin *plugin, GError **ret_error);

GType          goffice_plugin_get_type (void);
GOfficePlugin *goffice_plugin_new (const gchar *id_str, GOfficePluginCleanupFunc cleanup_func);
const gchar   *goffice_plugin_get_id (GOfficePlugin *plugin);
gboolean       goffice_plugin_cleanup (GOfficePlugin *plugin, GError **ret_error);

void           goffice_plugin_add_service (GOfficePlugin *plugin, GOfficeService *service);
void           goffice_plugin_remove_service (GOfficePlugin *plugin, GOfficeService *service);
const GList   *goffice_plugin_get_services (GOfficePlugin *plugin);
GList         *goffice_plugin_get_services_of_type (GOfficePlugin *plugin, GType type);

G_END_DECLS

#endif
