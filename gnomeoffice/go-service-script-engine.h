/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_OFFICE_SERVICE_SCRIPT_ENGINE_H__
#define __GNOME_OFFICE_SERVICE_SCRIPT_ENGINE_H__

#include <gnomeoffice/go-service-function-group.h>

G_BEGIN_DECLS

#define GOFFICE_TYPE_SERVICE_SCRIPT_ENGINE        (goffice_service_script_engine_get_type ())
#define GOFFICE_SERVICE_SCRIPT_ENGINE(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GOFFICE_TYPE_SERVICE_SCRIPT_ENGINE, GOfficeServiceScriptEngine))
#define GOFFICE_SERVICE_SCRIPT_ENGINE_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_SERVICE_SCRIPT_ENGINE, GOfficeServiceScriptEngineClass))
#define GOFFICE_IS_SERVICE_SCRIPT_ENGINE(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GOFFICE_TYPE_SERVICE_SCRIPT_ENGINE))
#define GOFFICE_IS_SERVICE_SCRIPT_ENGINE_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_SERVICE_SCRIPT_ENGINE))

typedef struct _GOfficeServiceScriptEngine      GOfficeServiceScriptEngine;
typedef struct _GOfficeServiceScriptEngineClass GOfficeServiceScriptEngineClass;

struct _GOfficeServiceScriptEngine {
	GOfficeServiceFunctionGroup service;
};

struct _GOfficeServiceScriptEngineClass {
	GOfficeServiceFunctionGroupClass parent_class;

	/* virtual methods */
	GList * (* service_get_languages) (GOfficeServiceScriptEngine *service);
	void (* service_execute) (GOfficeServiceScriptEngine *service, const gchar *source);
};

GType  goffice_service_script_engine_get_type (void);
GList *goffice_service_script_engine_get_languages (GOfficeServiceScriptEngine *service);
void   goffice_service_script_engine_execute (GOfficeServiceScriptEngine *service, const gchar *source);

#define GOFFICE_SERVICE_PLUGIN_LANGUAGE_CSHARP "C#"
#define GOFFICE_SERVICE_PLUGIN_LANGUAGE_PERL   "Perl"
#define GOFFICE_SERVICE_PLUGIN_LANGUAGE_PYTHON "Python"

G_END_DECLS

#endif
