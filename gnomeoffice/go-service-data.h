/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_OFFICE_SERVICE_DATA_H__
#define __GNOME_OFFICE_SERVICE_DATA_H__

#include <libgda/gda-data-model.h>
#include <libgda/gda-parameter.h>
#include <gnomeoffice/go-service.h>

G_BEGIN_DECLS

#define GOFFICE_TYPE_SERVICE_DATA        (goffice_service_data_get_type ())
#define GOFFICE_SERVICE_DATA(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GOFFICE_TYPE_SERVICE_DATA, GOfficeServiceData))
#define GOFFICE_SERVICE_DATA_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_SERVICE_DATA, GOfficeServiceDataClass))
#define GOFFICE_IS_SERVICE_DATA(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GOFFICE_TYPE_SERVICE_DATA))
#define GOFFICE_IS_SERVICE_DATA_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_SERVICE_DATA))

typedef struct _GOfficeServiceData      GOfficeServiceData;
typedef struct _GOfficeServiceDataClass GOfficeServiceDataClass;

struct _GOfficeServiceData {
	GOfficeService service;
};

struct _GOfficeServiceDataClass {
	GOfficeServiceClass parent_class;

	/* virtual methods */
	GdaDataModel * (* service_select) (GOfficeServiceData *service,
					   gboolean interactive,
					   GdaParameterList *params,
					   GError **ret_error);
};

GType         goffice_service_data_get_type (void);
GdaDataModel *goffice_service_data_select (GOfficeServiceData *service,
					   gboolean interactive,
					   GdaParameterList *params,
					   GError **ret_error);

G_END_DECLS

#endif
