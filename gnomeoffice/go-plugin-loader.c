/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <glib/gdir.h>
#include <gmodule.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <gnomeoffice/go-plugin-loader.h>

#define PARENT_TYPE G_TYPE_OBJECT

struct _GOfficePluginLoaderPrivate {
	GHashTable *loaded_modules;
};

typedef struct {
	GModule *handle;
	gchar *location;
	gchar *module_id;

	GOfficePlugin * (* plugin_create) (void);
} GOPL_ModuleInfo;

static void gopl_class_init (GOfficePluginLoaderClass *klass);
static void gopl_init (GOfficePluginLoader *loader, GOfficePluginLoaderClass *klass);
static void gopl_finalize (GObject *object);

static GObjectClass *parent_class = NULL;

static void
free_module_info (gpointer key, gpointer value, gpointer user_data)
{
	GOPL_ModuleInfo *module_info = (GOPL_ModuleInfo *) value;

	g_return_if_fail (module_info != NULL);

	g_module_close (module_info->handle);
	g_free (module_info->location);
	g_free (module_info->module_id);

	g_free (module_info);
}

static GOPL_ModuleInfo *
module_info_from_xml_file (const gchar *path)
{
	GOPL_ModuleInfo *module_info;
	xmlDocPtr doc;
	xmlNodePtr root, node;
	GModule *handle;
	gchar *location = NULL;
	gchar *module_id = NULL;

	/* load and parse the .plugin file */
	doc = xmlParseFile (path);
	if (!doc)
		return NULL;

	root = xmlDocGetRootElement (doc);
	if (!root || strcmp (root->name, "plugin")) {
		xmlFreeDoc (doc);
		return NULL;
	}

	for (node = root->xmlChildrenNode; node; node = node->next) {
		if (!strcmp (node->name, "location")) {
			if (location) {
				xmlFreeDoc (doc);
				g_warning ("'location' property specified more than once");
				return NULL;
			}

			location = xmlNodeGetContent (node);
		} else if (!strcmp (node->name, "id")) {
			if (module_id) {
				xmlFreeDoc (doc);
				g_warning ("'id' property specified more than once");
				return NULL;
			}

			module_id = xmlNodeGetContent (node);
		}
	}

	if (!location) {
		xmlFreeDoc (doc);
		g_warning ("No 'location' property in the plugin XML file");
		return NULL;
	}

	/* load the plugin */
	handle = g_module_open (location, 0);
	if (handle) {
		module_info = g_new0 (GOPL_ModuleInfo, 1);
		module_info->handle = handle;
		module_info->location = g_strdup (location);
		module_info->module_id = module_id ? g_strdup (module_id) : g_strdup (location);

		g_module_symbol (module_info->handle,
				 "go_plugin_create",
				 (gpointer *) &module_info->plugin_create);
		if (module_info->plugin_create) {
			xmlFreeDoc (doc);
			return module_info;
		}

		free_module_info (NULL, module_info, NULL);
	} 
	else
		g_warning (g_module_error ());

	xmlFreeDoc (doc);

	return NULL;
}

static void
gopl_class_init (GOfficePluginLoaderClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = gopl_finalize;
}

static void
gopl_init (GOfficePluginLoader *loader, GOfficePluginLoaderClass *klass)
{
	GDir *dir;
	const gchar *name;
	GError *err = NULL;

	/* initialize private structure */
	loader->priv = g_new0 (GOfficePluginLoaderPrivate, 1);
	loader->priv->loaded_modules = g_hash_table_new (g_str_hash, g_str_equal);

	/* load all available plugins */
	dir = g_dir_open (LIBGNOMEOFFICE_PLUGINCONFDIR, 0, &err);
	if (err) {
		g_error_free (err);
		return;
	}

	while ((name = g_dir_read_name (dir))) {
		gchar *path;
		GOPL_ModuleInfo *module_info;

		path = g_build_path (G_DIR_SEPARATOR_S,
				     LIBGNOMEOFFICE_PLUGINCONFDIR,
				     name, NULL);
		module_info = module_info_from_xml_file (path);
		if (module_info) {
			g_hash_table_insert (loader->priv->loaded_modules,
					     module_info->module_id,
					     module_info);
		}

		g_free (path);
	}

	g_dir_close (dir);
}

static void
gopl_finalize (GObject *object)
{
	GOfficePluginLoader *loader = (GOfficePluginLoader *) object;

	/* free memory */
	g_hash_table_foreach (loader->priv->loaded_modules, (GHFunc) free_module_info, NULL);
	g_hash_table_destroy (loader->priv->loaded_modules);
	loader->priv->loaded_modules = NULL;

	g_free (loader->priv);
	loader->priv = NULL;

	/* call parent class */
	parent_class->finalize (object);
}

GType
goffice_plugin_loader_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GOfficePluginLoaderClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gopl_class_init,
                        NULL,
                        NULL,
                        sizeof (GOfficePluginLoader),
                        0,
                        (GInstanceInitFunc) gopl_init
                };
                type = g_type_register_static (G_TYPE_OBJECT,
					       "GOfficePluginLoader",
					       &info, 0);
        }
        return type;
}

/**
 * goffice_plugin_loader_new
 *
 * Create a new #GOfficePluginLoader object, which allows applications
 * to load any kind of GNOME Office plugins.
 *
 * Returns: the newly-created object.
 */
GOfficePluginLoader *
goffice_plugin_loader_new (void)
{
	GOfficePluginLoader *loader;

	loader = g_object_new (GOFFICE_TYPE_PLUGIN_LOADER, NULL);
	return loader;
}

static void
add_module_id (gpointer key, gpointer value, gpointer user_data)
{
	GList **idlist = (GList **) user_data;

	*idlist = g_list_append (*idlist, g_strdup ((const gchar *) key));
}

/**
 * goffice_plugin_loader_get_ids
 * @loader: A #GOfficePluginLoader object.
 *
 * Return the IDs of all the available plugins.
 *
 * Returns: a GList of strings, each of which represent an ID
 * of a loaded plugin. You should free this value when you no
 * longer need it.
 */
GList *
goffice_plugin_loader_get_ids (GOfficePluginLoader *loader)
{	
	GList *idlist = NULL;

	g_return_val_if_fail (GOFFICE_IS_PLUGIN_LOADER (loader), NULL);

	g_hash_table_foreach (loader->priv->loaded_modules, (GHFunc) add_module_id, &idlist);

	return idlist;
}

/**
 * goffice_plugin_loader_activate
 * @loader: A #GOfficePluginLoader object.
 * @id: ID of the plugin to load.
 *
 * Activate a plugin given its unique ID.
 *
 * Returns: a pointer to the activated plugin. When you no longer need
 * it, free it by calling g_object_unref.
 */
GOfficePlugin *
goffice_plugin_loader_activate (GOfficePluginLoader *loader, const gchar *id)
{
	GOfficePlugin *plugin = NULL;
	GOPL_ModuleInfo *module_info;

	g_return_val_if_fail (GOFFICE_IS_PLUGIN_LOADER (loader), NULL);
	g_return_val_if_fail (id != NULL, NULL);

	module_info = g_hash_table_lookup (loader->priv->loaded_modules, id);
	if (module_info)
		plugin = module_info->plugin_create ();

	return plugin;
}
