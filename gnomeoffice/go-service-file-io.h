/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_OFFICE_SERVICE_FILE_IO_H__
#define __GNOME_OFFICE_SERVICE_FILE_IO_H__

#include <gsf/gsf-input.h>
#include <gsf/gsf-output.h>
#include <gnomeoffice/go-service.h>

G_BEGIN_DECLS

#define GOFFICE_TYPE_SERVICE_FILE_IO        (goffice_service_file_io_get_type ())
#define GOFFICE_SERVICE_FILE_IO(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GOFFICE_TYPE_SERVICE_FILE_IO, GOfficeServiceFileIo))
#define GOFFICE_SERVICE_FILE_IO_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_SERVICE_FILE_IO, GOfficeServiceFileIoClass))
#define GOFFICE_IS_SERVICE_FILE_IO(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GOFFICE_TYPE_SERVICE_FILE_IO))
#define GOFFICE_IS_SERVICE_FILE_IO_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), GOFFICE_TYPE_SERVICE_FILE_IO))

typedef struct _GOfficeServiceFileIo      GOfficeServiceFileIo;
typedef struct _GOfficeServiceFileIoClass GOfficeServiceFileIoClass;

struct _GOfficeServiceFileIo {
	GOfficeService service;
};

struct _GOfficeServiceFileIoClass {
	GOfficeServiceClass parent_class;

	/* virtual methods */
	gboolean (* plugin_file_open) (GOfficeServiceFileIo *service,
				       GsfInput *input,
				       GError **error);
	gboolean (* plugin_file_save) (GOfficeServiceFileIo *service,
				       GsfOutput *output,
				       GError **error);
};

GType    goffice_service_file_io_get_type (void);
gboolean goffice_service_file_io_open (GOfficeServiceFileIo *service,
				       GsfInput *input,
				       GError **error);
gboolean goffice_service_file_io_save (GOfficeServiceFileIo *service,
				       GsfOutput *output,
				       GError **error);

G_END_DECLS

#endif
