/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gnomeoffice/go-service-data.h>
#include <glib/gi18n.h>

#define PARENT_TYPE GOFFICE_TYPE_SERVICE
#define DATA_CLASS(service) GOFFICE_SERVICE_DATA_CLASS (G_OBJECT_GET_CLASS (service))

static void gosd_class_init (GOfficeServiceDataClass *klass);
static void gosd_init       (GOfficeServiceData *service,
			     GOfficeServiceDataClass *klass);
static void gosd_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

static void
gosd_class_init (GOfficeServiceDataClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = gosd_finalize;
	klass->service_select = NULL;
}

static void
gosd_init (GOfficeServiceData *service,
	   GOfficeServiceDataClass *klass)
{
}

static void
gosd_finalize (GObject *object)
{
	parent_class->finalize (object);
}

GType
goffice_service_data_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GOfficeServiceDataClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gosd_class_init,
                        NULL,
                        NULL,
                        sizeof (GOfficeServiceData),
                        0,
                        (GInstanceInitFunc) gosd_init
                };
                type = g_type_register_static (PARENT_TYPE,
					       "GOfficeServiceData",
					       &info, 0);
        }
        return type;
}

/**
 * goffice_service_data_select
 * @service: The service implementation.
 * @interactive: Whether the service is run interactively or not. If interactive,
 * the service can decide to pop up dialogs to ask the user for the needed
 * arguments.
 * @params: Parameters for the query.
 * @ret_error: Placeolder for error information.
 *
 * Returns: a GdaDataModel containing the data resulting from the
 * execution of the given query.
 */
GdaDataModel *
goffice_service_data_select (GOfficeServiceData *service,
			     gboolean interactive,
			     GdaParameterList *params,
			     GError **ret_error)
{
	g_return_val_if_fail (GOFFICE_IS_SERVICE_DATA (service), NULL);

	if (DATA_CLASS (service)->service_select != NULL)
		return DATA_CLASS (service)->service_select (service, interactive, params, ret_error);
	else {
		if (ret_error) {
			*ret_error = g_error_new_literal (
				g_quark_from_string (LIBGNOMEOFFICE_ERROR_DOMAIN),
				-1, _("Method not implemented"));
		}
	}

	return NULL;
}
