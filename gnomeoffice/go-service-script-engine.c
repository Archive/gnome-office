/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnomeoffice/go-service-script-engine.h>

#define PARENT_TYPE GOFFICE_TYPE_SERVICE_FUNCTION_GROUP
#define SCRIPT_ENGINE_CLASS(service) GOFFICE_SERVICE_SCRIPT_ENGINE_CLASS (G_OBJECT_GET_CLASS (service))

static void gosse_class_init (GOfficeServiceScriptEngineClass *klass);
static void gosse_init       (GOfficeServiceScriptEngine *service,
			      GOfficeServiceScriptEngineClass *klass);
static void gosse_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

static void
gosse_class_init (GOfficeServiceScriptEngineClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gosse_finalize;
	klass->service_execute = NULL;
}

static void
gosse_init (GOfficeServiceScriptEngine *service, GOfficeServiceScriptEngineClass *klass)
{
}

static void
gosse_finalize (GObject *object)
{
	parent_class->finalize (object);
}

GType
goffice_service_script_engine_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GOfficeServiceScriptEngineClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gosse_class_init,
                        NULL,
                        NULL,
                        sizeof (GOfficeServiceScriptEngine),
                        0,
                        (GInstanceInitFunc) gosse_init
                };
                type = g_type_register_static (PARENT_TYPE,
					       "GOfficeServiceScriptEngine",
					       &info, 0);
        }
        return type;
}

/**
 * goffice_service_script_engine_get_languages
 * @service: A #GOfficeServiceScriptEngine object.
 *
 * Get the list of supported scripting languages on the given
 * script engine service.
 *
 * Returns: a GList containing the names of all the languages.
 * When no longer needed, it should be freed, by calling the
 * #goffice_util_free_string_list.
 */
GList *
goffice_service_script_engine_get_languages (GOfficeServiceScriptEngine *service)
{
	g_return_val_if_fail (GOFFICE_IS_SERVICE_SCRIPT_ENGINE (service), NULL);

	if (SCRIPT_ENGINE_CLASS (service)->service_get_languages)
		return SCRIPT_ENGINE_CLASS (service)->service_get_languages (service);

	return NULL;
}

/**
 * goffice_service_script_engine_execute
 * @service: A #GOfficeServiceScriptEngine object.
 * @source: Filename of the script to execute.
 *
 * Execute a script in the given scripting engine. The @source parameter
 * specifies the file name from which to load the script.
 */
void
goffice_service_script_engine_execute (GOfficeServiceScriptEngine *service,
				       const gchar *source)
{
	g_return_if_fail (GOFFICE_IS_SERVICE_SCRIPT_ENGINE (service));
	g_return_if_fail (source != NULL);

	if (SCRIPT_ENGINE_CLASS (service)->service_execute)
		SCRIPT_ENGINE_CLASS (service)->service_execute (service, source);
}
