/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __LIBGNOMEOFFICE_H__
#define __LIBGNOMEOFFICE_H__

#include <gnomeoffice/go-plugin.h>
#include <gnomeoffice/go-plugin-loader.h>
#include <gnomeoffice/go-service.h>
#include <gnomeoffice/go-service-data.h>
#include <gnomeoffice/go-service-file-io.h>
#include <gnomeoffice/go-service-function-group.h>
#include <gnomeoffice/go-service-script-engine.h>

#endif
