/* GNOME Office library
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <gnomeofficeui/go-plugin-manager-dialog.h>
#include <glib/gi18n.h>
#include <gtk/gtkbox.h>
#include <gtk/gtkcellrenderertext.h>
#include <gtk/gtkcellrenderertoggle.h>
#include <gtk/gtkliststore.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkstock.h>
#include <glade/glade-xml.h>

#define PARENT_TYPE GTK_TYPE_DIALOG

enum {
	PROP_0,
	PROP_LOADER,
};

/* enumeration to specify the types for the plugin list */
enum {
	PL_COL_NAME,
	PL_COL_ACTIVATED,
	PL_N_COLUMNS
};

struct _GOfficePluginManagerDialogPrivate {
	GOfficePluginLoader *plugin_loader;

	/* the widgets in the dialog */
	GladeXML *ui;
	GtkWidget *notebook;
	GtkWidget *plugin_list;
	GtkWidget *plugin_description;
	GtkWidget *activate_plugins_default;
};

static void gopmd_class_init (GOfficePluginManagerDialogClass *klass);
static void gopmd_init       (GOfficePluginManagerDialog *dialog, GOfficePluginManagerDialogClass *klass);
static void gopmd_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

static void
add_plugin(gchar const* name, GtkListStore* store)
{
	GtkTreeIter iter;
	g_return_if_fail (GTK_IS_LIST_STORE (store));

	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,
			    PL_COL_NAME, name,
			    -1);
}

static void
gopmd_set_loader (GOfficePluginManagerDialog* self, GOfficePluginLoader* loader)
{
	GList       * plugins;
	GtkListStore* store;
	
	g_return_if_fail (GOFFICE_IS_PLUGIN_MANAGER_DIALOG (self));
	g_return_if_fail (GOFFICE_IS_PLUGIN_LOADER (loader));
	g_return_if_fail (self->priv->plugin_loader == NULL);

	self->priv->plugin_loader = g_object_ref(loader);

	store = GTK_LIST_STORE (gtk_tree_view_get_model (GTK_TREE_VIEW (self->priv->plugin_list)));
	gtk_list_store_clear (store);
	plugins = goffice_plugin_loader_get_ids (self->priv->plugin_loader);
	g_list_foreach (plugins, (GFunc)add_plugin, store);
	g_list_foreach (plugins, (GFunc)g_free, NULL);
	g_list_free (plugins);

	g_object_notify (G_OBJECT (self), "plugin-loader");
}

static void
gopmd_set_property (GObject* object, guint prop_id, const GValue* value, GParamSpec* pspec)
{
	g_return_if_fail (GOFFICE_IS_PLUGIN_MANAGER_DIALOG (object));

	switch (prop_id)
	{
	case PROP_LOADER:
		gopmd_set_loader (GOFFICE_PLUGIN_MANAGER_DIALOG (object), g_value_get_object (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static GOfficePluginLoader*
gopmd_get_loader (GOfficePluginManagerDialog* self)
{
	g_return_val_if_fail (GOFFICE_IS_PLUGIN_MANAGER_DIALOG (self), NULL);

	return self->priv->plugin_loader;
}

static void
gopmd_get_property (GObject* object, guint prop_id, GValue* value, GParamSpec* pspec)
{
	g_return_if_fail (GOFFICE_IS_PLUGIN_MANAGER_DIALOG (object));

	switch (prop_id)
	{
	case PROP_LOADER:
		g_value_set_object (value, gopmd_get_loader (GOFFICE_PLUGIN_MANAGER_DIALOG (object)));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
gopmd_class_init (GOfficePluginManagerDialogClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gopmd_finalize;
	object_class->set_property = gopmd_set_property;
	object_class->get_property = gopmd_get_property;

	g_object_class_install_property(object_class,
					PROP_LOADER,
					g_param_spec_object("plugin-loader",
							    "Plugin Loader",
							    "The Plugin Loader that's interfaced by this dialog",
							    GOFFICE_TYPE_PLUGIN_LOADER,
							    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

static void
gopmd_init_plugin_list (GOfficePluginManagerDialog *self)
{
	GtkCellRenderer  * renderer;
	GtkTreeModel     * model;
	GtkTreeViewColumn* column;
	
	g_return_if_fail (GOFFICE_IS_PLUGIN_MANAGER_DIALOG (self));

	/* set up the tree view */
	self->priv->plugin_list = glade_xml_get_widget (self->priv->ui, "plugin-list");

	renderer = gtk_cell_renderer_toggle_new ();
	g_object_set (G_OBJECT (renderer), "activatable", FALSE, NULL);
	/*g_signal_connect (renderer, "toggled",
		          G_CALLBACK (cb_cell_renderer_toggled), self);*/
	column = gtk_tree_view_column_new_with_attributes (_("Activated"),
							   renderer,
							   "active", PL_COL_ACTIVATED,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->plugin_list), column);
	column = gtk_tree_view_column_new_with_attributes (_("Plugin Name"),
							   gtk_cell_renderer_text_new (),
							   "text", PL_COL_NAME,
							   NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (self->priv->plugin_list), column);

	/* set up the tree view selection */
	/*g_signal_connect (gtk_tree_view_get_selection (GTK_TREE_VIEW (self->priv->plugin_list)), "changed",
			  G_CALLBACK (cb_plugin_list_selection_changed), self);*/

	/* set up and connect the model */
	model = GTK_TREE_MODEL (gtk_list_store_new(PL_N_COLUMNS,
						   G_TYPE_STRING,
						   G_TYPE_BOOLEAN));
	gtk_tree_view_set_model (GTK_TREE_VIEW (self->priv->plugin_list), model);
}

static void
gopmd_init (GOfficePluginManagerDialog *dialog, GOfficePluginManagerDialogClass *klass)
{
	dialog->priv = g_new0 (GOfficePluginManagerDialogPrivate, 1);

	dialog->priv->ui = glade_xml_new (LIBGNOMEOFFICE_GLADEDIR "/go-plugin-manager-dialog.glade",
					  "plugin-manager-main", NULL);
	if (!dialog->priv->ui)
		return;

	dialog->priv->notebook = glade_xml_get_widget (dialog->priv->ui, "plugin-manager-main");
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), dialog->priv->notebook, TRUE, TRUE, 6);

	gopmd_init_plugin_list (dialog);
	dialog->priv->plugin_description = glade_xml_get_widget (dialog->priv->ui, "plugin-description");
	dialog->priv->activate_plugins_default = glade_xml_get_widget (dialog->priv->ui,
								       "activate-plugins-default");

	gtk_window_set_title (GTK_WINDOW (dialog), _("Plugin Manager"));
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
}

static void
gopmd_finalize (GObject *object)
{
	GOfficePluginManagerDialog *dialog = (GOfficePluginManagerDialog *) object;

	g_return_if_fail (GOFFICE_PLUGIN_MANAGER_DIALOG (dialog));

	/* free memory */
	if (GOFFICE_IS_PLUGIN_LOADER (dialog->priv->plugin_loader)) {
		g_object_unref (dialog->priv->plugin_loader);
		dialog->priv->plugin_loader = NULL;
	}

	if (dialog->priv->ui) {
		g_object_unref (dialog->priv->ui);
		dialog->priv->ui = NULL;
	}

	g_free (dialog->priv);
	dialog->priv = NULL;

	parent_class->finalize (object);
}

GType
goffice_plugin_manager_dialog_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (GOfficePluginManagerDialogClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) gopmd_class_init,
                        NULL,
                        NULL,
                        sizeof (GOfficePluginManagerDialog),
                        0,
                        (GInstanceInitFunc) gopmd_init
                };
                type = g_type_register_static (PARENT_TYPE,
					       "GOfficePluginManagerDialog",
					       &info, 0);
        }
        return type;
}

/**
 * goffice_plugin_manager_dialog_new
 * @loader: A #GOfficePluginLoader object to use for retrieving plugins.
 * @parent: Parent window for the plugin manager dialog.
 *
 * Create a new #GOfficePluginManagerDialog, which is a self-contained
 * dialog to manage all the plugins in the gnome-office architecture.
 *
 * Returns: the newly created dialog.
 */
GtkWidget *
goffice_plugin_manager_dialog_new (GOfficePluginLoader *loader,
				   GtkWindow *parent)
{
	GOfficePluginManagerDialog *dialog;

	dialog = g_object_new (GOFFICE_TYPE_PLUGIN_MANAGER_DIALOG, "plugin-loader", loader, NULL);

	if (GTK_IS_WINDOW (parent))
		gtk_window_set_transient_for (GTK_WINDOW (dialog), parent);

	return (GtkWidget *) dialog;
}
