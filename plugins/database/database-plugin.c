/* GNOME Office database plugin
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gnomeoffice/go-plugin.h>
#include <libgda/libgda.h>
#include "data-service.h"

GOfficePlugin *go_plugin_create (void);

static gboolean plugin_initialized = FALSE;
static GOfficePlugin *db_plugin = NULL;;

/*
 * Private functions
 */

static gboolean
plugin_cleanup (GOfficePlugin *plugin, GError **ret_error)
{
	/* clean up everything we created */
	plugin_initialized = FALSE;

	return TRUE;
}

/*
 * Public functions
 */

GOfficePlugin *
go_plugin_create (void)
{
	if (!plugin_initialized) {
		gda_init ("goffice-database-plugin", VERSION, 0, NULL);

		/* create the plugin */
		db_plugin = goffice_plugin_new ("Database plugin", plugin_cleanup);
		goffice_plugin_add_service (db_plugin,
					    GOFFICE_SERVICE (data_service_new ()));

		plugin_initialized = TRUE;
	} else
		g_object_ref (G_OBJECT (db_plugin));

	return db_plugin;
}
