/* GNOME Office database plugin
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <glib/gi18n.h>
#include "data-service.h"
#ifdef HAVE_LIBGNOMEDB
#include "data-service-ui.h"
#endif

#define PARENT_TYPE GOFFICE_TYPE_SERVICE_DATA

static void data_service_class_init (DataServiceClass *klass);
static void data_service_init       (DataService *service, DataServiceClass *klass);
static void data_service_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

static void
set_error_gda (GError **ret_error, GdaConnection *cnc)
{
	if (ret_error) {
		const GList *error_list;

		error_list = gda_connection_get_errors (cnc);
		if (error_list) {
			*ret_error = g_error_new_literal (
				g_quark_from_string (LIBGNOMEOFFICE_ERROR_DOMAIN),
				-1, gda_error_get_description (error_list->data));
		}
	}
}

static void
set_error_string (GError **ret_error, const gchar *msg)
{
	if (ret_error) {
		*ret_error = g_error_new_literal (
			g_quark_from_string (LIBGNOMEOFFICE_ERROR_DOMAIN),
			-1, msg);
	}
}

static const gchar  *
data_service_get_description (GOfficeService *service)
{
	return _("Data access service for GNOME Office");
}

static const gchar  *
data_service_get_help (GOfficeService *service)
{
	return _("Help - FIXME");
}

static GdaDataModel *
data_service_select (GOfficeServiceData *sd, gboolean interactive, GdaParameterList *params, GError **ret_error)
{
	GdaParameter *par;
	gchar *dsn, *user = NULL, *password = NULL, *sql;
	GdaConnection *cnc;
	GdaCommand *cmd;
	GdaDataModel *model = NULL;
	DataService *service = (DataService *) sd;

	g_return_val_if_fail (IS_DATA_SERVICE (service), NULL);
	g_return_val_if_fail (params != NULL, NULL);

	/* get parameters */
	par = gda_parameter_list_find (params, "DSN");
	if (!par && !interactive) {
		set_error_string (ret_error, _("DSN parameter needed, but not found"));
		return NULL;
	}
	dsn = gda_value_get_string (gda_parameter_get_value (par));

	par = gda_parameter_list_find (params, "SQL");
	if (!par && !interactive) {
		set_error_string (ret_error, _("No SQL command has been specified"));
		return NULL;
	}
	sql = gda_value_get_string (gda_parameter_get_value (par));

	par = gda_parameter_list_find (params, "USER");
	if (par)
		user = gda_value_get_string (gda_parameter_get_value (par));

	par = gda_parameter_list_find (params, "PASSWORD");
	if (par)
		password = gda_value_get_string (gda_parameter_get_value (par));

	/* open connection to database */
#ifdef HAVE_LIBGNOMEDB
	if (interactive) {
		model = data_service_ui_select (service->db_client,
						dsn, user, password);
	} else {
#endif
		cnc = gda_client_open_connection (service->db_client, dsn, user, password,
						  GDA_CONNECTION_OPTIONS_READ_ONLY);
		if (!GDA_IS_CONNECTION (cnc)) {
			set_error_gda (ret_error, cnc);
			return NULL;
		}

		/* execute the command */
		cmd = gda_command_new (sql, GDA_COMMAND_TYPE_SQL, 0);
		model = gda_connection_execute_single_command (cnc, cmd, NULL);
		if (!model)
			set_error_gda (ret_error, cnc);

		/* free memory */
		gda_command_free (cmd);
#ifdef HAVE_LIBGNOMEDB
	}
#endif

	return model;
}

static void
data_service_class_init (DataServiceClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GOfficeServiceClass *serv_class = GOFFICE_SERVICE_CLASS (klass);
	GOfficeServiceDataClass *sd_class = GOFFICE_SERVICE_DATA_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = data_service_finalize;
	serv_class->service_get_description = data_service_get_description;
	serv_class->service_get_help = data_service_get_help;
	sd_class->service_select = data_service_select;
}

static void
data_service_init (DataService *service, DataServiceClass *klass)
{
	service->db_client = gda_client_new ();
}

static void
data_service_finalize (GObject *object)
{
	DataService *service = DATA_SERVICE (object);

	g_return_if_fail (IS_DATA_SERVICE (service));

	if (GDA_IS_CLIENT (service->db_client)) {
		g_object_unref (G_OBJECT (service->db_client));
		service->db_client = NULL;
	}

	/* call parent class */
	parent_class->finalize (object);
}

GType
data_service_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (DataServiceClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) data_service_class_init,
                        NULL,
                        NULL,
                        sizeof (DataService),
                        0,
                        (GInstanceInitFunc) data_service_init
                };
                type = g_type_register_static (PARENT_TYPE, "DataService", &info, 0);
        }
        return type;
}

GOfficeService *
data_service_new (void)
{
	DataService *service;

	service = g_object_new (TYPE_DATA_SERVICE, NULL);
	return GOFFICE_SERVICE (service);
}
