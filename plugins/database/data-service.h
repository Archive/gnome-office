/* GNOME Office database plugin
 * Copyright (C) 2002 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __DATA_SERVICE_H__
#define __DATA_SERVICE_H__

#include <gnomeoffice/go-service-data.h>
#include <libgda/gda-client.h>

G_BEGIN_DECLS

#define TYPE_DATA_SERVICE        (data_service_get_type ())
#define DATA_SERVICE(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), TYPE_DATA_SERVICE, DataService))
#define DATA_SERVICE_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), TYPE_DATA_SERVICE, DataServiceClass))
#define IS_DATA_SERVICE(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), TYPE_DATA_SERVICE))
#define IS_DATA_SERVICE_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), TYPE_DATA_SERVICE))

typedef struct {
	GOfficeServiceData service;

	GdaClient *db_client;
} DataService;

typedef struct {
	GOfficeServiceDataClass parent_class;
} DataServiceClass;

GType           data_service_get_type (void);
GOfficeService *data_service_new (void);

G_END_DECLS

#endif
