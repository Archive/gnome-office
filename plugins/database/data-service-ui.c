/* GNOME Office database plugin
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <glib/gi18n.h>
#include <libgda/gda-config.h>
#include <gtk/gtkstock.h>
#include <libgnomedb/gnome-db-grid.h>
#include <libgnomedb/gnome-db-login-dialog.h>
#include <libgnomedb/gnome-db-util.h>
#include "data-service-ui.h"

static void
create_widgets (GtkWidget *dialog)
{
}

GdaDataModel *
data_service_ui_select (GdaClient *client,
			const gchar *dsn,
			const gchar *username,
			const gchar *password)
{
	GdaDataModel *model = NULL;
	GtkWidget *dialog;

	/* create dialog */
	dialog = gtk_dialog_new_with_buttons (_("Data Sources"), NULL, 0,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OK, GTK_RESPONSE_OK,
					      NULL);
	create_widgets (dialog);

	/* run the dialog */
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
	}

	gtk_widget_destroy (dialog);

	return model;
}
