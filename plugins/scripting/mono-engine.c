/* GNOME Office scripting plugin
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <mono/jit/jit.h>
#include <glib/gi18n.h>
#include "mono-engine.h"

#define PARENT_TYPE GOFFICE_TYPE_SERVICE_SCRIPT_ENGINE

static void mono_engine_class_init (MonoEngineClass *klass);
static void mono_engine_init       (MonoEngine *service, MonoEngineClass *klass);
static void mono_engine_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

static const gchar  *
mono_engine_get_description (GOfficeService *service)
{
	return _("Mono scripting service for GNOME Office");
}

static const gchar  *
mono_engine_get_help (GOfficeService *service)
{
	return _("Help - FIXME");
}

static const gchar *
mono_engine_fg_get_category (GOfficeServiceFunctionGroup *service)
{
	return _("Scripting Language");
}

static GValue *
mono_engine_fg_eval (GOfficeServiceFunctionGroup *service, const gchar *fn_name, const GValueArray *values)
{
	return NULL;
}

static GList *
mono_engine_se_get_languages (GOfficeServiceScriptEngine *service)
{
	GList *list = NULL;

	list = g_list_append (list, g_strdup (GOFFICE_SERVICE_PLUGIN_LANGUAGE_CSHARP));
	return list;
}

static void
mono_engine_se_execute (GOfficeServiceScriptEngine *service, const gchar *source)
{
	MonoDomain *domain;
	MonoAssembly *assembly;

	g_return_if_fail (GOFFICE_IS_SERVICE_SCRIPT_ENGINE (service));
	g_return_if_fail (source != NULL);

	domain = mono_jit_init (source);
	assembly = mono_domain_assembly_open (domain, source);
	if (!assembly) {
		g_warning (_("Could not load assembly from %s"), source);
		mono_jit_cleanup (domain);
		return;
	}

	mono_jit_exec (domain, assembly, 0, NULL);

	mono_jit_cleanup (domain);
}

static void
mono_engine_class_init (MonoEngineClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GOfficeServiceClass *serv_class = GOFFICE_SERVICE_CLASS (klass);
	GOfficeServiceFunctionGroupClass *fg_class = GOFFICE_SERVICE_FUNCTION_GROUP_CLASS (klass);
	GOfficeServiceScriptEngineClass *se_class = GOFFICE_SERVICE_SCRIPT_ENGINE_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = mono_engine_finalize;
	serv_class->service_get_description = mono_engine_get_description;
	serv_class->service_get_help = mono_engine_get_help;
	fg_class->service_get_category = mono_engine_fg_get_category;
	fg_class->service_eval = mono_engine_fg_eval;
	se_class->service_get_languages = mono_engine_se_get_languages;
	se_class->service_execute = mono_engine_se_execute;
}

static void
mono_engine_init (MonoEngine *service, MonoEngineClass *klass)
{
}

static void
mono_engine_finalize (GObject *object)
{
	MonoEngine *service = MONO_ENGINE (object);

	g_return_if_fail (IS_MONO_ENGINE (service));

	/* call parent class */
	parent_class->finalize (object);
}

GType
mono_engine_get_type (void)
{
        static GType type = 0;

        if (!type) {
                static const GTypeInfo info = {
                        sizeof (MonoEngineClass),
                        (GBaseInitFunc) NULL,
                        (GBaseFinalizeFunc) NULL,
                        (GClassInitFunc) mono_engine_class_init,
                        NULL,
                        NULL,
                        sizeof (MonoEngine),
                        0,
                        (GInstanceInitFunc) mono_engine_init
                };
                type = g_type_register_static (PARENT_TYPE, "MonoEngine", &info, 0);
        }
        return type;
}

GOfficeService *
mono_engine_new (void)
{
	MonoEngine *service;

	service = g_object_new (TYPE_MONO_ENGINE, NULL);
	return GOFFICE_SERVICE (service);
}
