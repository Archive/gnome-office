/* GNOME Office scripting plugin
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __MONO_ENGINE_H__
#define __MONO_ENGINE_H__

#include <gnomeoffice/go-service-script-engine.h>

G_BEGIN_DECLS

#define TYPE_MONO_ENGINE        (mono_engine_get_type ())
#define MONO_ENGINE(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), TYPE_MONO_ENGINE, MonoEngine))
#define MONO_ENGINE_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), TYPE_MONO_ENGINE, MonoEngineClass))
#define IS_MONO_ENGINE(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), TYPE_MONO_ENGINE))
#define IS_MONO_ENGINE_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), TYPE_MONO_ENGINE))

typedef struct {
	GOfficeServiceScriptEngine service;
} MonoEngine;

typedef struct {
	GOfficeServiceScriptEngineClass parent_class;
} MonoEngineClass;

GType           mono_engine_get_type (void);
GOfficeService *mono_engine_new (void);

G_END_DECLS

#endif
