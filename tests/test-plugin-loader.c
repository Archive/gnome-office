/* GNOME Office test programs
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnomeoffice/go-plugin-loader.h>
#include <gnomeoffice/go-service-data.h>
#include <gnomeoffice/go-service-file-io.h>
#include <gnomeoffice/go-service-function-group.h>
#include <gnomeoffice/go-service-script-engine.h>

static void list_ids (GOfficePluginLoader *loader);

int
main (int argc, char *argv[])
{
	GOfficePluginLoader *loader;

	/* initialization */
	g_type_init ();

	/* create plugin loader */
	loader = goffice_plugin_loader_new ();

	/* tests */
	list_ids (loader);

	/* terminate */
	g_object_unref (G_OBJECT (loader));

	return 0;
}

static void
print_string (gpointer value, gpointer user_data)
{
	g_print (" %s", (const gchar *) value);
}


static void
print_string_and_free (gpointer value, gpointer user_data)
{
	g_print (" %s", (const gchar *) value);
	g_free (value);
}

static void
list_service_types (GOfficeService *service)
{
	GList *list;
	gchar *str;

	g_return_if_fail (GOFFICE_IS_SERVICE (service));

	g_print ("\tService '%s'\n\t\tImplements 'GOfficeService'\n", goffice_service_get_description (service));
	if (GOFFICE_IS_SERVICE_DATA (service))
		g_print ("\t\tImplements 'GOfficeServiceData'\n"); 
	else if (GOFFICE_IS_SERVICE_FILE_IO (service))
		g_print ("\t\tImplements 'GOfficeServiceFileIo'\n");
	else if (GOFFICE_IS_SERVICE_FUNCTION_GROUP (service)) {
		g_print ("\t\tImplements 'GOfficeServiceFunctionGroup'\n");

		str = goffice_service_function_group_get_category (service);
		g_print ("\t\t\tContains functions for '%s' category\n", str);

		list = goffice_service_function_group_get_names (service);
		if (list) {
			g_print ("\t\t\tFunctions:");
			g_list_foreach (list, (GFunc) print_string_and_free, NULL);
			g_list_free (list);
			g_print ("\n");
		}

		if (GOFFICE_IS_SERVICE_SCRIPT_ENGINE (service)) {
			g_print ("\t\tImplements 'GOfficeServiceScriptEngine'\n");

			list = goffice_service_script_engine_get_languages (service);
			if (list) {
				g_print ("\t\t\tSupported scripting languages:");
				g_list_foreach (list, (GFunc) print_string, NULL);
				g_list_free (list);
				g_print ("\n");
			}
		}
	}
}

static void
list_ids (GOfficePluginLoader *loader)
{
	GList *idlist;
	GList *l;

	g_print ("Getting list of plugins...\n");

	idlist = goffice_plugin_loader_get_ids (loader);
	for (l = idlist; l; l = l->next) {
		GOfficePlugin *plugin;
		const GList *services, *sl;
		gchar *id = (gchar *) l->data;

		g_print ("Found plugin with ID '%s'\n", id);
		plugin = goffice_plugin_loader_activate (loader, id);
		if (!plugin) {
			g_print ("\tERROR: Could not activate plugin with ID '%s'\n", id);
			continue;
		}

		services = goffice_plugin_get_services (plugin);
		if (services) {
			for (sl = services; sl != NULL; sl = sl->next) {
				list_service_types ((GOfficeService *) sl->data);
			}
		} else {
			g_print ("\tNo services in this plugin\n");
		}

		g_object_unref (G_OBJECT (plugin));
		g_print ("\n");
	}

	g_list_foreach (idlist, (GFunc) g_free, NULL);
	g_list_free (idlist);
}
