/* GNOME Office test programs
 * Copyright (C) 2002 The GNOME Foundation.
 * Copyright (C) 2005 Sven Herzberg
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Sven Herzberg <herzi@gnome-de.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnomeofficeui/go-plugin-manager-dialog.h>

static void
dialog_destroyed_cb (GtkWidget *widget, gpointer user_data)
{
	gtk_main_quit ();
}

static void
dialog_response_cb (GtkDialog *dialog, gint response_id, gpointer user_data)
{
	gtk_widget_destroy (GTK_WIDGET (dialog));
}

int
main (int argc, char *argv[])
{
	GOfficePluginLoader *loader;
	GOfficePluginManagerDialog *dialog;

	gtk_init (&argc, &argv);

	loader = goffice_plugin_loader_new ();
	dialog = goffice_plugin_manager_dialog_new (loader, NULL);

	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (dialog_response_cb), NULL);
	g_signal_connect (G_OBJECT (dialog), "destroy",
			  G_CALLBACK (dialog_destroyed_cb), NULL);
	gtk_widget_show (dialog);

	gtk_main ();

	g_object_unref (loader);

	return 0;
}
